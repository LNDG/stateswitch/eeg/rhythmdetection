clear all; clc;

%% initialize

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S5D_eBOSC_CSD_Stim/';
pn.bosc = [pn.root, 'B_data/A_eBOSC/'];
pn.dataOut = [pn.root, 'A_scripts/B_extractIndices/B_data/'];
pn.tools = [pn.root, 'A_scripts/B_extractIndices/T_tools/']; addpath(genpath(pn.tools));

%% get IDs

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

fileNames = strcat(IDs, repmat('_bosc.mat',numel(IDs),1));

IDs = cellfun(@str2num,IDs);

for indID = 1:length(IDs)
    % define ID
    ID = num2str([IDs(indID,1)]); disp(ID);
    % load config-structure
    load([pn.bosc fileNames{indID}], 'BGinfo');
    for indCond = 1:4
        MergedBG_log10(indID,indCond,:,:) = BGinfo.all.(['dim',num2str(indCond),'_bg_log10_pow']);
        MergedBG(indID,indCond,:,:) = BGinfo.all.(['dim',num2str(indCond),'_bg_pow']);
        MergedMP(indID,indCond,:,:) = BGinfo.all.(['dim',num2str(indCond),'_mp']);
    end
end
    
freq = 2.^[0:.125:6];

figure; imagesc(BGinfo.all.dim3_bg_log10_pow-BGinfo.all.dim1_bg_log10_pow)
figure; imagesc(BGinfo.all.dim4_bg_log10_pow-BGinfo.all.dim1_bg_log10_pow)
figure; imagesc(BGinfo.all.dim4_mp-BGinfo.all.dim1_mp)

figure; hold on;
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,1,55:60,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,2,55:60,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,3,55:60,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,4,55:60,:),3),1)))

figure; hold on;
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,1,1:10,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,1,11:20,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,1,21:30,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,1,31:40,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,1,41:50,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,1,51:60,:),3),1)))

figure; hold on;
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,4,1:10,:)-MergedBG_log10(:,1,1:10,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,4,11:20,:)-MergedBG_log10(:,1,11:20,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,4,21:30,:)-MergedBG_log10(:,1,21:30,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,4,31:40,:)-MergedBG_log10(:,1,31:40,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,4,41:50,:)-MergedBG_log10(:,1,41:50,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,4,51:60,:)-MergedBG_log10(:,1,51:60,:),3),1)))
xticks([5:5:45])
xticklabels(round(freq(5:5:45),1));
xlabel('Frequency')

figure; imagesc(squeeze(nanmean(MergedBG_log10(:,1,:,:),1)))
xticks([5:5:45])
xticklabels(round(freq(5:5:45),1));
xlabel('Frequency')

figure; imagesc(squeeze(nanmean(MergedMP(:,1,55:60,:),3)))
xticks([5:5:45])
xticklabels(round(freq(5:5:45),1));
xlabel('Frequency')

figure; imagesc(squeeze(nanmean(MergedBG_log10(:,1,55:60,:),3)))
xticks([5:5:45])
xticklabels(round(freq(5:5:45),1));
xlabel('Frequency')

MergedBG_thresh = MergedBG;
MergedBG_thresh(MergedBG<MergedMP) = NaN;

figure; imagesc(squeeze(nanmean(MergedBG_thresh(:,1,55:60,:),3)))
xticks([5:5:45])
xticklabels(round(freq(5:5:45),1));
xlabel('Frequency')

figure; imagesc(squeeze(nanmedian(MergedBG_log10(:,1,50:60,:),3)))
figure; imagesc(squeeze(nanmedian(MergedBG_log10(:,1,8:12,:),3)))

figure; imagesc(corrcoef([squeeze(nanmedian(MergedBG_log10(:,1,40:50,:),3)), ...
    squeeze(nanmedian(MergedBG_log10(:,1,50:60,:),3))]))

figure; imagesc(squeeze(nanmedian(MergedBG(:,1,50:60,:),3)))

figure; hold on;
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,1,8:11,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,2,8:11,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,3,8:11,:),3),1)))
plot(squeeze(nanmean(nanmean(MergedBG_log10(:,4,8:11,:),3),1)))

figure; hold on;
plot(squeeze(nanmean(nanmean(MergedBG(:,1,44:60,:),1),3)))
plot(squeeze(nanmean(nanmean(MergedBG(:,2,44:60,:),1),3)))
plot(squeeze(nanmean(nanmean(MergedBG(:,3,44:60,:),1),3)))
plot(squeeze(nanmean(nanmean(MergedBG(:,4,44:60,:),1),3)))


find(freq>29 & freq<32)