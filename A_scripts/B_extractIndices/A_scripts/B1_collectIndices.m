%% extract eBOSC's various indices of rhythmic content

clear all; clc;

%% initialize

pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S5D_eBOSC_CSD_Stim/';
pn.bosc = [pn.root, 'B_data/A_eBOSC/'];
pn.dataOut = [pn.root, 'A_scripts/B_extractIndices/B_data/'];
pn.tools = [pn.root, 'A_scripts/B_extractIndices/T_tools/']; addpath(genpath(pn.tools));

%% get IDs

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

fileNames = strcat(IDs, repmat('_bosc.mat',numel(IDs),1));

IDs = cellfun(@str2num,IDs);

%%  loop ids & get IAF

cond = {'dim1','dim2','dim3','dim4'};
condlab = {'dim1','dim2','dim3','dim4'};

%% different frequencies

FrequencyCategories = {'Delta'; 'Theta'; 'Alpha'; 'Beta'; 'Gamma1'; 'Gamma2'};
FreqLimits = [2.5, 4; 4, 8; 8, 15; 15, 25; 25, 40; 40, 64];
FrequencyRanges = {'Theta'; 'Theta'; 'Alpha'; 'Beta'; 'Gamma'; 'Gamma'};
FrequencySubRange = {'A';'D'; 'D'; 'C'; 'D';'D'};

for id = 1:length(IDs)

    % define ID
    ID = num2str([IDs(id,1)]); disp(ID);
    % load config-structure
    load([pn.bosc fileNames{id}]);

    %% info for extraction

    info.channels       = 1:60; % encode for all channels
    info.freq           = cfg.eBOSC.F; % frequency vector
    info.frequencyROI   = [8 15]; % temporary frequency vector
    info.alphaIDX       = find(info.freq>=info.frequencyROI(1) & info.freq<=info.frequencyROI(2));
    info.alphaFREQS     = info.freq(info.alphaIDX);

    %% get overall data
        
        % data overall (average across trials)
        % NOTE: These spectra are calculated only across the
        % length of the analyzed segment.
        dim1o = spctrm.dim1.o_amp;
            dim1o = cat(3, dim1o{:});    
        dim2o = spctrm.dim2.o_amp;
            dim2o = cat(3, dim2o{:});
        dim3o = spctrm.dim3.o_amp;
            dim3o = cat(3, dim3o{:});
        dim4o = spctrm.dim3.o_amp;
            dim4o = cat(3, dim4o{:});
            
        for indFreq = 1:numel(FrequencyCategories)
            
            currentFreq = FrequencyCategories{indFreq};
            currentRange = FrequencyRanges{indFreq};
            currentSubRange = FrequencySubRange{indFreq};
            currentLims = FreqLimits(indFreq,:);
            
            %% get info from rhythm-conditional spectra

            % Spectrum across all sample points at which an episode with
            % mean frequency in the above range was detected.
             
            dim1r = spctrm.dim1.([currentSubRange, '_', currentRange, '_amp']);
                dim1r = cat(3, dim1r{:});  
            dim2r = spctrm.dim2.([currentSubRange, '_', currentRange, '_amp']);
                dim2r = cat(3, dim2r{:});    
            dim3r = spctrm.dim3.([currentSubRange, '_', currentRange, '_amp']);
                dim3r = cat(3, dim3r{:});    
            dim4r = spctrm.dim4.([currentSubRange, '_', currentRange, '_amp']);
                dim4r = cat(3, dim4r{:});   
                
            % non-rhythmic spectra
            dim1nr = spctrm.dim1.([currentSubRange, '_No', currentRange, '_amp']);
                dim1nr = cat(3, dim1nr{:});    
            dim2nr = spctrm.dim2.([currentSubRange, '_No', currentRange, '_amp']);
                dim2nr = cat(3, dim2nr{:});    
            dim3nr = spctrm.dim3.([currentSubRange, '_No', currentRange, '_amp']);
                dim3nr = cat(3, dim3nr{:});    
            dim4nr = spctrm.dim4.([currentSubRange, '_No', currentRange, '_amp']);
                dim4nr = cat(3, dim4nr{:}); 
                
            % rhythm-conditional spectrum (difference)
            dim1rd = dim1r-dim1nr;    
            dim2rd = dim2r-dim2nr;    
            dim3rd = dim3r-dim3nr;
            dim4rd = dim4r-dim4nr;  

            %% info for extraction

            % implement loop for different frequencies

            info.channels       = 1:60; % encode for all channels
            info.freq           = cfg.eBOSC.F; % frequency vector
            info.frequencyROI   = currentLims; % temporary frequency vector
            info.freqIDX       = find(info.freq>=info.frequencyROI(1) & info.freq<=info.frequencyROI(2));
            info.freqFREQS     = info.freq(info.freqIDX);

            %% IAF and amplitude for alpha episodes and overall

            % Procedure: Average alpha episode/overall spectrum at selected channels within IAF range to determine IAF.
            % The amplitude at the IAF is extracted; the BGdiff version subtracts the 
            % average fitted BG amplitude across channels from the IAF amplitude.

            % When spectra refer to alpha episodes, spectra result
            % from all sample points at which an episode with mean
            % frequency in the alpha range is indicated.

            % Resulting matrices are of the form id x indTrial.

            spectra = {'o'; 'r'; 'rd'}; % overall, rhythm-cond., rhythm-cond. difference

            for indSpectra = 1:numel(spectra)
            for indCond = 1:size(cond,2)
                X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_iaf'])(id,:,:)           = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_amp'])(id,:,:)           = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_amp_BGdiff'])(id,:,:)    = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_amp_BGrel'])(id,:,:)     = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_fitBG'])(id,:,:)         = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_NaNmat'])(id,:,:)        = NaN(numel(info.channels),70);
            for indTrial = 1:size(eval([cond{indCond} ,spectra{indSpectra}]),3)
                for indChannel = 1:numel(info.channels)
                    e = info.channels(indChannel); % select current electrode
                    eval(['tmp = ' cond{indCond} ,spectra{indSpectra}, '(e,info.freqIDX,indTrial);']) % spectrum within condition and indTrial at selected channels within IAF range
                    mx = THG_find_local_max_1D_20150117(tmp,1); % index of IAF peak
                    %X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_trials'])(id,1) = 1; % amount of trials
                    if max(~isnan(tmp))==0
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_iaf'])(id,e,indTrial) = NaN;
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_amp'])(id,e,indTrial) = NaN;
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_amp_BGdiff'])(id,e,indTrial) = NaN;
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_amp_BGrel'])(id,e,indTrial) = NaN;
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_fitBG'])(id,e,indTrial) = NaN;
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_NaNmat'])(id,e,indTrial) = 1;
                    else
                        % decide what to do in the case of multiple peaks
                        if length(mx) == 1
                            mx = mx;
                        elseif length(mx) > 1 % multiple peaks
                            mx = mx(tmp(mx)==max(tmp(mx))); % largest IAF peak
                            mx = mx(1);
                        elseif length(mx) < 1 % no peak
                            mx = find(tmp==max(tmp)); % largest IAF peak
                            mx = mx(1);
                        end
                        % IAF
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_iaf'])(id,e,indTrial) = info.freqFREQS(mx);
                        % absolute amplitude
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_amp'])(id,e,indTrial) = tmp(mx);
                        % difference measure (from BG)
                        bgamp = mean(sqrt(BGinfo.all.([condlab{indCond}, '_mp'])(e,info.freqIDX(mx)))); % mean fitted BG spectrum at current channel & IAF
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_amp_BGdiff'])(id,e,indTrial) = tmp(mx)-bgamp;
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_amp_BGrel'])(id,e,indTrial) = (tmp(mx)-bgamp)./bgamp;
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_fitBG'])(id,e,indTrial) = bgamp;
                        X.(currentFreq).([cond{indCond}, spectra{indSpectra}, '_NaNmat'])(id,e,indTrial) = 0;
                    end
                    clear tmp mx
                end; clear indChannel

            end; clear indTrial
            end; clear indCond
            end; clear indSpectra

            %% IAF, amplitude and abundance of (!) rhythmic episodes

            % encode segment length to calculate abundance

            timeVec = cfg.eBOSC.waveseg(1)+1:1/cfg.eBOSC.fsample:cfg.eBOSC.waveseg(2);
            timeVec = timeVec(cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad);
            segLength = numel(timeVec);

            % NOTE: episodes already correspond to the correct segment
            % length (see ~line 83).

            for indCond = 1:size(cond,2)

                % intiate empty matrices to save indTrial-wise info (account for unequal amounts of trials)
                X.(currentFreq).([cond{indCond}, 'e_iaf'])(id,:,:)           = NaN(numel(info.channels),70); % encode up to 70 trials
                X.(currentFreq).([cond{indCond}, 'e_fitBG'])(id,:,:)         = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_PT'])(id,:,:)            = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_amp'])(id,:,:)           = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_amp_BGdiff'])(id,:,:)    = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_amp_BGrel'])(id,:,:)     = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_abn'])(id,:,:)           = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_NaNs'])(id,:,:)          = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_Pep'])(id,:,:)           = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_Pep_freq'])(id,:,:)      = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_abn_THG'])(id,:,:)       = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_NaNmat'])(id,:,:)        = NaN(numel(info.channels),70);
                X.(currentFreq).([cond{indCond}, 'e_amp_std'])(id,:,:)       = NaN(numel(info.channels),70);

                for indTrial = 1:size(episodes.(condlab{indCond}),2)
                    tmp.(cond{indCond}) = []; % initate structure
                    tmp.([cond{indCond}, '_abn']) = []; % initate structure
                    tmp.([cond{indCond}, '_BGdiff']) = [];
                    % NOTE: matrix concatenates results across all channels
                    % within identified range
                    for indChannel = 1:numel(info.channels)
                        e = info.channels(indChannel); % select current electrode
                        % get currently relevant episodes
                        curEpisodes = episodes.(condlab{indCond}){1,indTrial}{e,1};
                        % encoding loop
                        if isempty(curEpisodes) % no episodes were found
                            tmp.(cond{indCond})                   = [NaN, NaN];                            
                            tmp.([cond{indCond}, '_fitBG'])       = NaN;
                            tmp.([cond{indCond}, '_PT'])          = NaN;
                            tmp.([cond{indCond}, '_BGdiff'])      = [NaN, NaN];
                            tmp.([cond{indCond}, '_BGrel'])       = [NaN, NaN];
                            tmp.([cond{indCond}, '_abn'])         = 0;
                            tmp.([cond{indCond}, '_abn_THG'])     = NaN;
                            tmp.([cond{indCond}, '_Pep'])         = 0;
                            tmp.([cond{indCond}, '_Pep_freq'])    = 0;
                            tmp.([cond{indCond}, '_NaNmat'])      = 1;
                            tmp.([cond{indCond}, '_AmpStd'])      = NaN;
                        else
                            %% Pepisode
                            % for the Pepisode equivalent, put all indices
                            % together and calculate the numel for each
                            % freq
                            if iscell(curEpisodes)
                                tmp_det = cat(1, curEpisodes{:,1});
                            else tmp_det = curEpisodes;
                            end
                            tmp_detMat = zeros(size(info.freq,2), segLength);
                            tmp_detMat(tmp_det(:,1), tmp_det(:,2)) = 1;
                            tmp_Pepisode = mean(tmp_detMat,2);
                            % encode maximum Pepisode in frequency range
                            [P_episode, idx_max] = max(tmp_Pepisode(info.freqIDX));
                            if max(tmp_Pepisode(info.freqIDX)) == 0 % pepisode is zero --> excluded
                                tmp.([cond{indCond}, '_Pep']) = NaN;
                                tmp.([cond{indCond}, '_Pep_freq']) = NaN;
                            else 
                                P_ep_freq = info.freq(info.freqIDX(1)+idx_max-1);
                                tmp.([cond{indCond}, '_Pep']) = P_episode;
                                tmp.([cond{indCond}, '_Pep_freq']) = P_ep_freq;
                            end
                            clear tmp_* P_episode P_ep_freq idx_max;
                            tmp.idx = cell2mat(curEpisodes(:,3))>= info.freqFREQS(1) & cell2mat(curEpisodes(:,3))<= info.freqFREQS(end);
                            if max(tmp.idx) == 0 % if no episodes were found in the range
                                tmp.(cond{indCond}) = [NaN, NaN];
                                tmp.([cond{indCond}, '_fitBG']) = NaN;
                                tmp.([cond{indCond}, '_PT']) = NaN;
                                tmp.([cond{indCond}, '_BGdiff']) = [NaN, NaN];
                                tmp.([cond{indCond}, '_BGrel']) = [NaN, NaN];
                                tmp.([cond{indCond}, '_abn']) = 0;
                                tmp.([cond{indCond}, '_abn_THG']) = 0;
                                tmp.([cond{indCond}, '_NaNmat']) = 1;
                                tmp.([cond{indCond}, '_AmpStd']) = NaN;
                            else
                                %% frequency & amplitude
                                matchingEpisodes = cell2mat(curEpisodes(tmp.idx,2));
                                frequency = matchingEpisodes(:,1);
                                amplitude = sqrt(matchingEpisodes(:,2)); % square root of power values
                                %% row & column indices
                                Rows_Columns = cell2mat(curEpisodes(tmp.idx,1));
                                rows = Rows_Columns(:,1);
                                columns = Rows_Columns(:,2);
                                % unique column indices
                                uniqueSPs = unique(columns);
                                %% power spectrum of fitted BG
                                fitBG = sqrt(BGinfo.all.([condlab{indCond}, '_mp'])(e,:));
                                % part of fitted BG that corresponds to frequencies of detected points
                                selectBG = fitBG(rows)';
                                tmp.([cond{indCond}, '_fitBG']) = nanmean(selectBG); 
                                %% power threshold
                                PT = BGinfo.all.([condlab{indCond}, '_pt'])(e,:);
                                selectPT = PT(rows);
                                tmp.([cond{indCond}, '_PT']) = nanmean(selectPT); 
                                % absolute amplitude within episode
                                tmp.(cond{indCond}) = [nanmean(frequency,1), nanmean(amplitude,1)];
                                tmp.([cond{indCond}, '_AmpStd']) = nanstd(amplitude,[],1);
                                % difference of detected amplitude from fitted BG
                                tmp.([cond{indCond}, '_BGdiff'])  = [nanmean(frequency,1), nanmean(amplitude-selectBG,1)]; 
                                tmp.([cond{indCond}, '_BGrel'])   = [nanmean(frequency,1), nanmean((amplitude-selectBG)./selectBG,1)];  
                                tmp.([cond{indCond}, '_abn'])     = numel(uniqueSPs)/segLength;
                                tmp.([cond{indCond}, '_abn_THG']) = numel(Rows_Columns(:,2))/segLength;
                                tmp.([cond{indCond}, '_NaNmat'])  = 0;
                                clear frequency amplitude Rows_Columns rows columns uniqueSPs fitBG selectBG;
                            end
                        end
                        % enter single-indTrial indices into structure
                        X.(currentFreq).([cond{indCond}, 'e_iaf'])(id,e,indTrial) = tmp.(cond{indCond})(:,1);
                        X.(currentFreq).([cond{indCond}, 'e_amp'])(id,e,indTrial) = tmp.(cond{indCond})(:,2);
                        X.(currentFreq).([cond{indCond}, 'e_fitBG'])(id,e,indTrial) = tmp.([cond{indCond}, '_fitBG']);
                        X.(currentFreq).([cond{indCond}, 'e_PT'])(id,e,indTrial) = tmp.([cond{indCond}, '_PT']);
                        X.(currentFreq).([cond{indCond}, 'e_amp_BGdiff'])(id,e,indTrial) = tmp.([cond{indCond},'_BGdiff'])(:,2);
                        X.(currentFreq).([cond{indCond}, 'e_amp_BGrel'])(id,e,indTrial) = tmp.([cond{indCond},'_BGrel'])(:,2);
                        X.(currentFreq).([cond{indCond}, 'e_abn'])(id,e,indTrial) = tmp.([cond{indCond},'_abn']);
                        X.(currentFreq).([cond{indCond}, 'e_abn_THG'])(id,e,indTrial) = tmp.([cond{indCond}, '_abn_THG']);
                        X.(currentFreq).([cond{indCond}, 'e_Pep'])(id,e,indTrial) = tmp.([cond{indCond},'_Pep']);
                        X.(currentFreq).([cond{indCond}, 'e_Pep_freq'])(id,e,indTrial) = tmp.([cond{indCond},'_Pep_freq']);
                        X.(currentFreq).([cond{indCond}, 'e_NaNs'])(id,e,indTrial) = tmp.([cond{indCond}, '_NaNmat']);
                        X.(currentFreq).([cond{indCond}, 'e_amp_std'])(id,e,indTrial) = tmp.([cond{indCond}, '_AmpStd']);
                        clear tmp;
                    end; clear e indChannel;
                end; clear indTrial;
            end; clear indCond;
            
        end

    % The output is a id x trial structure for each condition.

    % clear variables
    clear EC1o EC2o EO1o EO2o EC1a EC2a EO1a EO2a freq* tmp* tmp_*

end; clear id

% save X (results structure)
save([pn.dataOut, 'X.mat'], 'X', '-v7.3');
