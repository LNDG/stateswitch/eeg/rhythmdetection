% plot topographies for all metrics, do stats tests @ second level

%% get data

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S5D_eBOSC_CSD_Stim_v2/';
pn.plotFolder = [pn.root, 'A_scripts/C_interrelations/C_figures/'];
pn.bosc = [pn.root, 'B_data/A_eBOSCout/'];
pn.Xdata = [pn.root, 'A_scripts/B_extractIndices/B_data/'];
pn.plotFolder = [pn.root, 'A_scripts/C_interrelations/C_figures/'];
pn.FieldTrip = [pn.root, 'T_tools/fieldtrip-20170904/'];
addpath(pn.FieldTrip); ft_defaults;

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools/RainCloudPlots'))
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools')

%% setup

% N = 47 YAs
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

info.channels = 1:60;

%% load task data

load([pn.Xdata, 'X.mat']);

cond = {'dim1','dim2','dim3','dim4'};

% h = figure('units','normalized','position',[.1 .1 .7 .9]);
% h2 = figure('units','normalized','position',[.1 .1 .7 .9]);
% 
frequencies = {'Delta'; 'Theta'; 'Alpha'; 'Beta'; 'Gamma1'; 'Gamma2'};

for indFreq = 1:numel(frequencies)
for c = 1:numel(cond)
    % single-trial amplitudes (avg. across channels); note there are no single trials for o & a
%     e_amp_singleTrial(:,c,:) = X{1,1}.Alpha.([cond{c},'e_amp'])(:,info.channels,:);
%     e_BG_singleTrial(:,c,:) = X{1,1}.Alpha.([cond{c},'e_fitBG'])(:,info.channels,:);
%     % single-trial abundance (avg. across channels)
%     e_abn_singleTrial(:,c,:) = X{1,1}.Alpha.([cond{c},'e_abn'])(:,info.channels,:);
    % trial average
    o_data(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'o_amp'])(:,info.channels,:),3)';
    a_data(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'r_amp'])(:,info.channels,:),3)';
    e_data(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'e_amp'])(:,info.channels,:),3)';
    
    o_BGfit(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'o_fitBG'])(:,info.channels,:),3)';
    a_BGfit(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'r_fitBG'])(:,info.channels,:),3)';
    e_BGfit(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'e_fitBG'])(:,info.channels,:),3)';
    
    abn_data(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'e_abn'])(:,info.channels,:),3)';

    % xAmp-BG
    oDiff(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'o_amp'])(:,info.channels,:) - ...
        X.(frequencies{indFreq}).([cond{c},'o_fitBG'])(:,info.channels,:),3)';
    aDiff(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'r_amp'])(:,info.channels,:) - ...
        X.(frequencies{indFreq}).([cond{c},'r_fitBG'])(:,info.channels,:),3)';
    eDiff(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'e_amp'])(:,info.channels,:) - ...
        X.(frequencies{indFreq}).([cond{c},'e_fitBG'])(:,info.channels,:),3)';
    
    % xAmp./BG
    oRel(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'o_amp'])(:,info.channels,:)./...
        X.(frequencies{indFreq}).([cond{c},'o_fitBG'])(:,info.channels,:),3)';
    aRel(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'r_amp'])(:,info.channels,:)./...
        X.(frequencies{indFreq}).([cond{c},'r_fitBG'])(:,info.channels,:),3)';
    eRel(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'e_amp'])(:,info.channels,:)./...
        X.(frequencies{indFreq}).([cond{c},'e_fitBG'])(:,info.channels,:),3)';
    
    % xAmp-BG./BG
    oDiffRel(indFreq,:,c,:) = nanmean((X.(frequencies{indFreq}).([cond{c},'o_amp'])(:,info.channels,:)-...
        X.(frequencies{indFreq}).([cond{c},'o_fitBG'])(:,info.channels,:))./...
        X.(frequencies{indFreq}).([cond{c},'o_fitBG'])(:,info.channels,:),3)';
    aDiffRel(indFreq,:,c,:) = nanmean((X.(frequencies{indFreq}).([cond{c},'r_amp'])(:,info.channels,:)-...
        X.(frequencies{indFreq}).([cond{c},'r_fitBG'])(:,info.channels,:))./...
        X.(frequencies{indFreq}).([cond{c},'r_fitBG'])(:,info.channels,:),3)';
    eDiffRel(indFreq,:,c,:) = nanmean((X.(frequencies{indFreq}).([cond{c},'e_amp'])(:,info.channels,:)-...
        X.(frequencies{indFreq}).([cond{c},'e_fitBG'])(:,info.channels,:))./...
        X.(frequencies{indFreq}).([cond{c},'e_fitBG'])(:,info.channels,:),3)';
    
    % mean frequency of episodes
    
    e_iaf(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'e_iaf'])(:,info.channels,:),3)';
    e_Pep(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'e_Pep'])(:,info.channels,:),3)';
    
    % power threshold
    
    e_PT(indFreq,:,c,:) = nanmean(X.(frequencies{indFreq}).([cond{c},'e_PT'])(:,info.channels,:),3)';
    
end
end

abn_data(isnan(abn_data)) = 0;
eDiffRel(isnan(eDiffRel)) = 1;
eDiff(isnan(eDiff)) = 0;

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/elec.mat')

subjects_YA = [1:47];

%% plot bar plots with significance

idxChanTheta = 10:12;
idxChanAlpha = 44:60;
idxChanAlphaPAC = 58:60;

%% occipital alpha abundance
    
    alphaMerged = squeeze(nanmean(abn_data(3,idxChanAlphaPAC,:,subjects_YA),2))'.*100;
    summaryIdx = 1:47;

    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = alphaMerged(summaryIdx,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = alphaMerged(summaryIdx,i)-...
                nanmean(alphaMerged(summaryIdx,:),2)+...
                repmat(nanmean(nanmean(alphaMerged(summaryIdx,:),2),1),size(alphaMerged(summaryIdx,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = [0.0314, 0.3176, 0.6118];

    h = figure('units','normalized','position',[.1 .1 .2 .3]);
    set(gcf,'renderer','Painters')

        cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
%         condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%         condPairsLevel = [75 77 80 83 86 89];
        condPairs = [1,2];
        condPairsLevel = [75];
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel({'Alpha abundance (%)'})
    % test linear effect
    curData = [data{1, 1}, data{2, 1}, data{3, 1}, data{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title({'Rhythmic alpha duration'; ['linear effect: p = ', num2str(round(p,3))]}); 
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    xlim([25 80]); 
    yticks = get(gca, 'ytick'); ylim([yticks(1)-.5*(yticks(2)-yticks(1)) yticks(end)+.5*(yticks(2)-yticks(1))]);

figureName = 'A2_AlphaAbnPAC';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% occipital alpha SNR: raincloud plot

    alphaMerged = squeeze(nanmean(eDiffRel(3,idxChanAlpha,:,subjects_YA),2))';
    summaryIdx = 1:47;

    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = alphaMerged(summaryIdx,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = alphaMerged(summaryIdx,i)-...
                nanmean(alphaMerged(summaryIdx,:),2)+...
                repmat(nanmean(nanmean(alphaMerged(summaryIdx,:),2),1),size(alphaMerged(summaryIdx,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = [0.0314, 0.3176, 0.6118];

    h = figure('units','normalized','position',[.1 .1 .2 .2]);
    set(gcf,'renderer','Painters')

        cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
%         condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%         condPairsLevel = [2.5 2.6 2.7 2.8 2.9 2.95]+.25;
        condPairs = [1,2];
        condPairsLevel = [2.75];
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel({'1/f-corrected alpha power'})
    % test linear effect
    curData = [data{1, 1}, data{2, 1}, data{3, 1}, data{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title({'Rhythmic alpha power'; ['linear effect: p = ', num2str(round(p,3))]}); 
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    xlim([1.5 3.2]);
    yticks = get(gca, 'ytick'); ylim([yticks(1)-.5*(yticks(2)-yticks(1)) yticks(end)+.5*(yticks(2)-yticks(1))]);
    
figureName = 'A1_AlphaSNROccipital';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
  
%% occipital alpha abundance
    
    alphaMerged = squeeze(nanmean(abn_data(3,idxChanAlpha,:,subjects_YA),2))'.*100;
    summaryIdx = 1:47;

    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = alphaMerged(summaryIdx,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = alphaMerged(summaryIdx,i)-...
                nanmean(alphaMerged(summaryIdx,:),2)+...
                repmat(nanmean(nanmean(alphaMerged(summaryIdx,:),2),1),size(alphaMerged(summaryIdx,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = [0.0314, 0.3176, 0.6118];

    h = figure('units','normalized','position',[.1 .1 .2 .2]);
    set(gcf,'renderer','Painters')

        cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
%         condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%         condPairsLevel = [75 77 80 83 86 89];
        condPairs = [1,2];
        condPairsLevel = [75];
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel({'Alpha abundance (%)'})
    % test linear effect
    curData = [data{1, 1}, data{2, 1}, data{3, 1}, data{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title({'Rhythmic alpha duration'; ['linear effect: p = ', num2str(round(p,3))]}); 
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    xlim([25 100]); 
    yticks = get(gca, 'ytick'); ylim([yticks(1)-.5*(yticks(2)-yticks(1)) yticks(end)+.5*(yticks(2)-yticks(1))]);

figureName = 'A2_AlphaAbnOccipital';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% frontal theta SNR: raincloud plot

    thetaMerged = squeeze(nanmean(eDiffRel(2,idxChanTheta,:,subjects_YA),2))';
    summaryIdx = 1:47;

    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = thetaMerged(summaryIdx,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = thetaMerged(summaryIdx,i)-...
                nanmean(thetaMerged(summaryIdx,:),2)+...
                repmat(nanmean(nanmean(thetaMerged(summaryIdx,:),2),1),size(thetaMerged(summaryIdx,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = [.3 .3 .3];

    h = figure('units','normalized','position',[.1 .1 .2 .2]);
    set(gcf,'renderer','Painters')

        cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
        condPairsLevel = [2.5 2.6 2.7 2.8 2.9 2.95]-1;
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel({'1/f-corrected theta power'})
    % test linear effect
    curData = [data{1, 1}, data{2, 1}, data{3, 1}, data{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title({'Rhythmic theta power'; ['linear effect: p = ', num2str(round(p,3))]}); 
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    xlim([1.4 2.2]);
    yticks = get(gca, 'ytick'); ylim([yticks(1)-.5*(yticks(2)-yticks(1)) yticks(end)+.5*(yticks(2)-yticks(1))]);

figureName = 'A3_ThetaSNRFrontal';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

  
%% frontal theta abundance
    
    thetaMerged = squeeze(nanmean(abn_data(2,idxChanTheta,:,subjects_YA),2))'.*100;
    summaryIdx = 1:47;

    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = thetaMerged(summaryIdx,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = thetaMerged(summaryIdx,i)-...
                nanmean(thetaMerged(summaryIdx,:),2)+...
                repmat(nanmean(nanmean(thetaMerged(summaryIdx,:),2),1),size(thetaMerged(summaryIdx,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    
    cl = 2.*[.3 .1 .1];

    h = figure('units','normalized','position',[.1 .1 .2 .2]);
    set(gcf,'renderer','Painters')

        cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
%         condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%         condPairsLevel = [50 52 54 56 58 60];
        condPairs = [1,2];
        condPairsLevel = [50];
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel({'Theta abundance (%)'})
    % test linear effect
    curData = [data{1, 1}, data{2, 1}, data{3, 1}, data{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title({'Rhythmic theta duration'; ['linear effect: p = ', num2str(round(p,3))]}); 
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    xlim([15 55]); 
    yticks = get(gca, 'ytick'); ylim([yticks(1)-.75*(yticks(2)-yticks(1)) yticks(end)+.75*(yticks(2)-yticks(1))]);

figureName = 'A4_ThetaAbnFrontal';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% save estimates

eBOSCestimates.rhythmicAlphaPower = squeeze(nanmean(eDiffRel(3,idxChanAlpha,:,subjects_YA),2))';
eBOSCestimates.rhythmicAlphaDuration = squeeze(nanmean(abn_data(3,idxChanAlpha,:,subjects_YA),2))'.*100;
eBOSCestimates.rhythmicThetaPower = squeeze(nanmean(eDiffRel(2,idxChanTheta,:,subjects_YA),2))';
eBOSCestimates.rhythmicThetaDuration = squeeze(nanmean(abn_data(2,idxChanTheta,:,subjects_YA),2))'.*100;
eBOSCestimates.IDs = IDs(subjects_YA);

pn.out = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S5D_eBOSC_CSD_Stim_v2/A_scripts/C_interrelations/B_data/';
save([pn.out, 'A_eBOSCestimates.mat'], 'eBOSCestimates');

%% topographies alpha, theta

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools/brewermap');
cBrew = brewermap(1000,'RdBu');
cBrew = double(flipud(cBrew));
cBrew = cBrew(501:end,:);

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.style = 'both';
cfg.colormap = cBrew;
cfg.zlim = 'maxmin';
cfg.marker = 'off';  
cfg.highlight = 'yes';
cfg.highlightcolor = [1 1 1];
cfg.highlightsymbol = '.';
cfg.highlightsize = 40;

plotData = [];
plotData.label = elec.label(1:60); % {1 x N}
plotData.dimord = 'chan';

% h = figure('units','normalized','position',[.1 .1 .7 .35]);
% for indFreq = 1:6
%     subplot(2,3,indFreq);
%     plotData.powspctrm = squeeze(nanmean(abn_data(indFreq,:,:,subjects_YA),4));
%     ft_topoplotER(cfg,plotData); 
% end

h = figure('units','normalized','position',[.1 .1 .7 .35]);
set(gcf,'renderer','Painters')
subplot(1,2,1);
cfg.highlightchannel = elec.label([idxChanTheta]);
plotData.powspctrm = squeeze(nanmean(nanmean(abn_data(2,:,1:4,subjects_YA),3),4))';
ft_topoplotER(cfg,plotData); 
subplot(1,2,2);
cfg.highlightchannel = elec.label([idxChanAlpha]);
%cfg.highlightchannel = elec.label([44:50, 52:56, 58:60]);
plotData.powspctrm = squeeze(nanmean(nanmean(abn_data(3,:,1:4,subjects_YA),3),4))';
ft_topoplotER(cfg,plotData); 

figureName = 'A5_ThetaAlphaTopos';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
