#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% add fieldtrip toolbox
addpath('/home/mpib/LNDG/StateSwitch/WIP_eeg/S5D_eBOSC_CSD_Stim/T_tools/fieldtrip-20170904/')
ft_defaults()
ft_compile_mex(true)
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP_eeg/S5D_eBOSC_CSD_Stim/A_scripts/A3_BOSC/')
%% compile function and append dependencies
mcc -m A3_SS_eBOSC_CSD_Stim_180919.m -a /home/mpib/LNDG/StateSwitch/WIP_eeg/S5D_eBOSC_CSD_Stim/T_tools/eBOSC-master/ -a /home/mpib/LNDG/StateSwitch/WIP_eeg/S5D_eBOSC_CSD_Stim/T_tools/fieldtrip-20170904/template/electrode/
exit