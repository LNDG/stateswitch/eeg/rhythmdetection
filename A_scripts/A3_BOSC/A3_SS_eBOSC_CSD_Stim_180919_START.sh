#!/bin/bash

# call the BOSC analysis by session and participant
cd /home/mpib/LNDG/StateSwitch/WIP_eeg/S5D_eBOSC_CSD_Stim/A_scripts/A3_BOSC/

subjectAmount=100

for ind_id in $(seq 1 $subjectAmount); do
	echo "#PBS -N STSWD_eBOSC_Stim_${ind_id}" 			> job
	echo "#PBS -l walltime=20:00:0" 				>> job
	echo "#PBS -l mem=8gb" 					    >> job
	echo "#PBS -j oe" 								>> job
	echo "#PBS -o /home/mpib/LNDG/StateSwitch/WIP_eeg/S5D_eBOSC_CSD_Stim/Y_logs" >> job
	echo "#PBS -m n" 								>> job
	echo "#PBS -d ." 								>> job
	echo "./A3_SS_eBOSC_CSD_Stim_180919_run.sh /opt/matlab/R2016b $ind_id" 	>> job
	qsub job
	rm job # delete job file after execution
done
