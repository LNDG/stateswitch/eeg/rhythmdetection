function A3_SS_eBOSC_CSD_Stim_180919(id)

% Conduct BOSC + eBOSC analysis

% 180201 | CSD transformation prior to eBOSC calculation

%% set paths

if ismac
    %pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/';
    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/';
    addpath([pn.root, 'S5D_eBOSC_CSD_Stim/T_tools/fieldtrip-20170904/']); ft_defaults;
    addpath(genpath([pn.root, 'S5D_eBOSC_CSD_Stim/T_tools/eBOSC-master/']));
    addpath([pn.root, 'S5D_eBOSC_CSD_Stim/T_tools/fieldtrip-20170904/template/electrode/']); % load template folder
else
    pn.root = '/home/mpib/LNDG/StateSwitch/WIP_eeg/';
    addpath('/home/mpib/LNDG/StateSwitch/WIP_eeg/S5D_eBOSC_CSD_Stim/T_tools/fieldtrip-20170904/template/electrode/'); % load template folder
end
pn.dataIn    = [pn.root, 'X1_preprocEEGData/'];
pn.boscOut   = [pn.root, 'S5D_eBOSC_CSD_Stim/B_data/A_eBOSC'];

%%  BOSC parameters

cfg.eBOSC.F                 = 2.^[0:.125:6];                            % frequency sampling (~Whitten et al., 2011), but higher frequency resolution
cfg.eBOSC.wavenumber        = 6;                                        % wavelet family parameter (time-frequency tradeoff) [recommended: ~6]
cfg.eBOSC.ncyc              = repmat(3, 1, numel(cfg.eBOSC.F));         % vector of duration thresholds at each frequency
cfg.eBOSC.percentile        = .95;                                      % percentile of background fit for power threshold
cfg.eBOSC.fsample           = 500;                                      % current sampling frequency of EEG data
cfg.eBOSC.WLpadding         = 1000;                                      % padding to avoid edge artifacts due to WL [SPs]
cfg.eBOSC.detectedPad       = 500;                                      % 'shoulder' for BOSC detected matrix to account for duration threshold
cfg.eBOSC.trialPad          = 1500;                                      % complete padding (WL + shoulder)
cfg.eBOSC.BGpad             = 1500;                                      % padding of segments for BG (only avoiding edge artifacts)
cfg.eBOSC.fstp              = 1;
cfg.eBOSC.BiasCorrection    = 'yes';                                    % use temporal correction for impact of wavelet?
cfg.eBOSC.method            = 'MaxBias';
cfg.eBOSC.edgeOnly          = 'yes';
cfg.eBOSC.effSignal         = 'PT';
cfg.eBOSC.LowFreqExcludeBG  = 8;                                        % lower bound of bandpass to be excluded prior to background fit
cfg.eBOSC.HighFreqExcludeBG = 15;                                       % higher bound of bandpass to be excluded prior to background fit
cfg.eBOSC.appliedPadding_s = (cfg.eBOSC.trialPad./cfg.eBOSC.fsample);
cfg.eBOSC.periodOfInterest = [3.5 6];
cfg.eBOSC.waveseg = [cfg.eBOSC.periodOfInterest(1)-cfg.eBOSC.appliedPadding_s, ...
    cfg.eBOSC.periodOfInterest(2)+cfg.eBOSC.appliedPadding_s];

Ranges.FrequencyCategories = {'Theta'; 'Alpha'; 'Beta'; 'Gamma'};
            
Ranges.Theta = cell(4,4);
Ranges.Theta(:,1) = {'A'; 'B'; 'C'; 'D'};
Ranges.Theta(:,2) = {'3 to 7 Hz';'3 to 8 Hz';'4 to 7 Hz';'4 to 8 Hz'};
Ranges.Theta(:,3) = num2cell([3;3;4;4]);
Ranges.Theta(:,4) = num2cell([7;8;7;8]);

Ranges.Alpha = cell(5,4);
Ranges.Alpha(:,1) = {'A'; 'B'; 'C'; 'D'; 'E'};
Ranges.Alpha(:,2) = {'7.5 to 12.5 Hz';'8.0 to 13.0 Hz';'8.0 to 12.0 Hz'; '8.0 to 15.0 Hz'; '6.0 to 15.0 Hz'};
Ranges.Alpha(:,3) = num2cell([7.5;8;8;8;6]);
Ranges.Alpha(:,4) = num2cell([12.5;13;12;15;15]);

Ranges.Beta = cell(4,4);
Ranges.Beta(:,1) = {'A'; 'B'; 'C'; 'D'};
Ranges.Beta(:,2) = {'15 to 20 Hz';'12 to 25 Hz';'15 to 25 Hz';'12 to 20 Hz'};
Ranges.Beta(:,3) = num2cell([15;12;15;13]);
Ranges.Beta(:,4) = num2cell([20;25;25;20]);

Ranges.Gamma = cell(4,4);
Ranges.Gamma(:,1) = {'A'; 'B'; 'C'; 'D'};
Ranges.Gamma(:,2) = {'30 to 64 Hz';'40 to 64 Hz';'50 to 64 Hz';'25 to 64 Hz'};
Ranges.Gamma(:,3) = num2cell([30;40;50;25]);
Ranges.Gamma(:,4) = num2cell([64;64;64;64]);

cfg.eBOSC.Ranges = Ranges;

%% load subject list

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%% get info about current subject

id = str2num(id); ID = IDs{id};
conditions= {'dim1';'dim2';'dim3';'dim4'};

%% load data

load([pn.dataIn,  IDs{id}, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'], 'data')

%% prepare data

    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;

    %% remove extraneous channels
    
    rem             = [];
    rem.channel     = {'all','-IOR','-LHEOG','-RHEOG','-A1','-A2'};
    rem.demean      = 'no';
    
    dataNew = ft_preprocessing(rem, data); clear rem data;
    dataNew.TrlInfo = TrlInfo;
    dataNew.TrlInfoLabels = TrlInfoLabels;
    
    %% define conditions and loop across them
        
    for c = 1:length(conditions)
    
        %% segment data according to condition

        tmp.idx = find([TrlInfo(:,8)] == str2num(conditions{c}(end)));
        tmp.NOidx = find([TrlInfo(:,8)] ~= str2num(conditions{c}(end)));

        % rebuild data.Reref according to condition data

        data.(conditions{c}) = dataNew;
        data.(conditions{c}).sampleinfo(tmp.NOidx,:) = [];
        data.(conditions{c}).trial(:,tmp.NOidx) = [];
        data.(conditions{c}).time(:,tmp.NOidx) = [];
        data.(conditions{c}).TrlInfo = TrlInfo(tmp.idx,:);
        data.(conditions{c}).TrlInfoLabels = TrlInfoLabels;

        TrlInfoCond = data.(conditions{c}).TrlInfo;
        TrlInfoLabelsCond = data.(conditions{c}).TrlInfoLabels;
        
        %% Redefine data to retention interval

        dataFull.(conditions{c}) = data.(conditions{c}); % save original for calculation of BG
        
        % data for BOSC (includes extensive peri-retention data)

        cfg.begsample = find(data.(conditions{c}).time{1,1} > cfg.eBOSC.waveseg(1), 1, 'first');
        cfg.endsample = find(data.(conditions{c}).time{1,1} <= cfg.eBOSC.waveseg(2), 1, 'last');
        
        data.(conditions{c}) = ft_redefinetrial(cfg, data.(conditions{c}));
        
        cfg.inputTime =  data.(conditions{c}).time{1,1};
        cfg.detectedTime = cfg.inputTime(cfg.eBOSC.WLpadding+1:end-cfg.eBOSC.WLpadding);
        cfg.finalTime = cfg.inputTime(cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad);
        
        %% apply CSD transformation
        
        % CSD transform
        csd_cfg = [];
        csd_cfg.elecfile = 'standard_1005.elc';
        csd_cfg.method = 'spline';
        data.(conditions{c}) = ft_scalpcurrentdensity(csd_cfg, data.(conditions{c}));
        data.(conditions{c}).TrlInfo = TrlInfoCond; clear TrlInfoCond;
        data.(conditions{c}).TrlInfoLabels = TrlInfoLabelsCond; clear TrlInfoLabelsCond;

    end; clear c; % condition loop

    %%%%%%%%%%%
    %% eBOSC %%
    %%%%%%%%%%%
    
    %% oscillation detection
        
    for e = 1:60 % electrode loop

        % display progress
        display([ID ', channel #' num2str(e)])
        tic
        
    %%  TF analysis for whole signal to prepare background fit
    
        B = [];
        for j = 1:length(conditions) % conditon index
        for t = 1:length(data.(conditions{j}).trial) % trial index
            % get data
            tmp_dat = data.(conditions{j}).trial{t}(e,:);
            % wavelet transform (NOTE: no check to avoid spectral leakage);
            % apply correction factor
            B.(conditions{j}).trial{t} = BOSC_tf(tmp_dat,cfg.eBOSC.F,cfg.eBOSC.fsample,cfg.eBOSC.wavenumber);
            clear tmp_dat
        end; clear t
        end; clear j
           
    %% condition-specific background fit --> power threshold estimation
    
        for c = 1:length(conditions)
            
            amountTrials = size(B.(conditions{c}).trial,2);
            
            BG = [];
            for t = 1:length(data.(conditions{c}).trial)
                % collect TF values; Remove BGpad at beginning and end.
                % Note that BGpad removes some edge segments to avoid 
                % potential edge artifacts, yet the final segment does not
                % exclusively cover the retention period.
                BG = [BG B.(conditions{c}).trial{t}(:,cfg.eBOSC.BGpad+1:end-cfg.eBOSC.BGpad)];
            end; clear t
            
            %% standard backgrounds
            
            % background power estimation - standard
            [pv_stnd,~] = BOSC_bgfit(cfg.eBOSC.F,BG); 
            mp_stnd = 10.^(polyval(pv_stnd,log10(cfg.eBOSC.F))); 

            %% robust background estimation
            
            % get lower and upper frequency index for requested range
            freqInd1 = find(cfg.eBOSC.F >= cfg.eBOSC.LowFreqExcludeBG, 1, 'first');
            freqInd2 = find(cfg.eBOSC.F <= cfg.eBOSC.HighFreqExcludeBG, 1, 'last');
            
            % find IAF between 8-15 Hz
            [~, indPos] = max(mean(BG(freqInd1:freqInd2,:),2));
            indPos = freqInd1+indPos;
            
            % consider wavelet extension in frequency domain
            LowFreq = cfg.eBOSC.F(indPos)-(((2/cfg.eBOSC.wavenumber)*cfg.eBOSC.F(indPos))/2);
            UpFreq = cfg.eBOSC.F(indPos)+(((2/cfg.eBOSC.wavenumber)*cfg.eBOSC.F(indPos))/2);
            
            % get frequency range for to-be-excluded frequencies
            freqIndLow = find(cfg.eBOSC.F >= LowFreq, 1, 'first');
            freqIndHigh = find(cfg.eBOSC.F <= UpFreq, 1, 'last');
            
            % remove any frequencies between 28 and 32 Hz (SSVEP)
            
            rmFreqs = [freqIndLow:freqIndHigh,find(cfg.eBOSC.F>=28 & cfg.eBOSC.F<=32)];
            fitFreqs = find(1-ismember([1:numel(cfg.eBOSC.F)],rmFreqs));
            
            % remove points within this range from the estimation; compute robust regression
            [pv,~] = eBOSC_bgfit_robust(cfg.eBOSC.F(fitFreqs),BG(fitFreqs, :));
            mp = 10.^(polyval(pv,log10(cfg.eBOSC.F))); 

            % BOSC thresholds (power + duration); based on robust background
            [pt,dt] = BOSC_thresholds(cfg.eBOSC.fsample,cfg.eBOSC.percentile,cfg.eBOSC.ncyc,cfg.eBOSC.F,mp);

            % keep overall background, 1/f fit, and power threshold
            BGinfo.all.([conditions{c},'_bg_pow'])(e,:)        = mean(BG(:,cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad),2);
            BGinfo.all.([conditions{c},'_bg_log10_pow'])(e,:)  = mean(log10(BG(:,cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad)),2);
            BGinfo.all.([conditions{c},'_bg_amp'])(e,:)        = mean(sqrt(BG(:,cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad)),2);
            BGinfo.all.([conditions{c},'_pv'])(e,:)            = pv;
            BGinfo.all.([conditions{c},'_mp'])(e,:)            = mp;
            BGinfo.all.([conditions{c},'_mp_stnd'])(e,:)       = mp_stnd;
            BGinfo.all.([conditions{c},'_pt'])(e,:)            = pt;

            % clear variables
            clear pv pv_stnd mp_stnd
            
            for t = 1:size(B.(conditions{c}).trial,2)

                % initialize variables
                bosc.pepisode.(conditions{c}){1,t}(e,:)  = zeros(1,size(cfg.eBOSC.F,2));
                bosc.abundance.(conditions{c}){1,t}(e,:) = zeros(1,size(cfg.eBOSC.F,2));
                bosc.episodes.(conditions{c}){1,t}{e,1}  = [];

                % get wavelet transform for current trial
                TF_all = B.(conditions{c}).trial{1,t};
                TF_WL = B.(conditions{c}).trial{1,t}(:,cfg.eBOSC.WLpadding+1:end-cfg.eBOSC.WLpadding);
                TF_Trl = B.(conditions{c}).trial{1,t}(:,cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad);

            %%  detect rhythms + calculate Pepisode (~standard BOSC)

                % BOSC oscillation detection
                % WLpadding is removed to avoid edge artifacts during the
                % detection. Note that detectedPad still remains so that there
                % is no problems with too few sample points at the edges to
                % fulfill the numcycles criterion.

                % oscillation detection
                detected = zeros(size(TF_WL));
                for f = 1:length(cfg.eBOSC.F)
                    detected(f,:) = BOSC_detect(TF_all(f,cfg.eBOSC.WLpadding+1:end-cfg.eBOSC.WLpadding),pt(f),dt(f),cfg.eBOSC.fsample);
                end; clear f

                % Pepisode = length of detected oscillation SPs relative to
                % segment length. Here, we discard detectedPad sample points 
                % at each end to account for the numcycles requirement.
                bosc.pepisode.(conditions{c}){1,t}(e,:) = mean(detected(:,cfg.eBOSC.detectedPad+1:end-cfg.eBOSC.detectedPad),2);

            %%  abundance estimation (THG's BOSC extension, adapted by JQK)

            	cfg.eBOSC.npnts = size(detected,2);
                cfg.eBOSC.pt = pt;
                episodes = [];
                [detected_abn,episodes] = eBOSC_createEpisodes(TF_WL,detected, cfg);                
                % remove fields
                cfg.eBOSC = rmfield(cfg.eBOSC,'npnts');
                cfg.eBOSC = rmfield(cfg.eBOSC,'pt');

                % The abundance measure here is simply the pepisode measure
                % after the extended detection. NOTE: This measure is not
                % currently used.

                bosc.abundance.(conditions{c}){1,t}(e,:) = mean(detected_abn(:,cfg.eBOSC.detectedPad+1:end-cfg.eBOSC.detectedPad),2);
                
                % Save overall spectral power of the segment                
                bosc.spctrm.(conditions{c}).o_pow{1,t}(e,:)       = mean(TF_Trl,2);
                bosc.spctrm.(conditions{c}).o_log10_pow{1,t}(e,:) = mean(log10(TF_Trl),2);
                bosc.spctrm.(conditions{c}).o_amp{1,t}(e,:)       = mean(sqrt(TF_Trl),2);

                %%  remove padded segments from episodes
                % The extended episode detection has as an input the padded
                % detected matrix. Therefore, the episodes that have been
                % defined based on the new extended detected matrix have to be
                % cut appropriately to discard the padding.

                ind1 = cfg.eBOSC.detectedPad+1;
                ind2 = size(detected_abn,2) - cfg.eBOSC.detectedPad;
                cnt = 1;
                rmv = [];
                for j = 1:size(episodes,1)
                    % final episodes
                    ex = find(episodes{j,1}(:,2) < ind1 | episodes{j,1}(:,2) > ind2);
                    % update episodes
                    episodes{j,1}(ex,:) = [];
                    episodes{j,1}(:,2) = episodes{j,1}(:,2) - ind1 + 1; 
                    episodes{j,2}(ex,:) = [];
                    episodes{j,3}       = mean(episodes{j,2}(:,1));
                    episodes{j,4}       = size(episodes{j,1},1) / cfg.eBOSC.fsample; clear ex
                    if isempty(episodes{j,1})
                        rmv(cnt,1) = j;
                        cnt = cnt + 1;
                    end
                end; clear j cnt
                episodes(rmv,:) = []; clear rmv

        %% save data in structure
        
            % episodes
            bosc.episodes.(conditions{c}){1,t}{e,1}  = episodes;

        %% encode results for different frequency ranges (+ different sub-bands for potential exploration)
            
            % go through each frequency range
            for indFreq = 1:numel(Ranges.FrequencyCategories)
                curFreqName = Ranges.FrequencyCategories{indFreq};
                % go through each sub-range
                for indRange = 1:size(Ranges.(curFreqName),1)
                    curPrefix = [Ranges.(curFreqName){indRange,1},'_',curFreqName];
                    curPrefixNo = [Ranges.(curFreqName){indRange,1},'_No',curFreqName];
                     % if there are ANY episodes that are detected
                    if sum(size(episodes)) > 0
                        % get episodes falling into requested frequencies
                        lowFreqThresh = Ranges.(curFreqName){indRange,3}; % lower threshold for frequency range
                        highFreqThresh = Ranges.(curFreqName){indRange,4}; % higher frequency thresholds
                        relevantEpisodes = cell2mat(episodes(find(cell2mat(episodes(:,3)) >= lowFreqThresh & cell2mat(episodes(:,3)) <= highFreqThresh),1));
                        % create index of detected timepoints
                        amountTimePoints = size(TF_Trl,2);
                        index = zeros(amountTimePoints,1);
                        if ~isempty(relevantEpisodes)
                            index(relevantEpisodes(:,2)) = 1;
                        end
                        % extract useful parameters
                        bosc.spctrm.(conditions{c}).([curPrefix,'_pow']){1,t}(e,:) = single(mean(TF_Trl(:,index==1),2));
                        bosc.spctrm.(conditions{c}).([curPrefixNo,'_pow']){1,t}(e,:) = single(mean(TF_Trl(:,index==0),2));
                        bosc.spctrm.(conditions{c}).([curPrefix,'_log10_pow']){1,t}(e,:) = single(mean(log10(TF_Trl(:,index==1)),2));
                        bosc.spctrm.(conditions{c}).([curPrefixNo,'_log10_pow']){1,t}(e,:) = single(mean(log10(TF_Trl(:,index==0)),2));
                        bosc.spctrm.(conditions{c}).([curPrefix,'_amp']){1,t}(e,:) = single(mean(sqrt(TF_Trl(:,index==1)),2));
                        bosc.spctrm.(conditions{c}).([curPrefixNo,'_amp']){1,t}(e,:) = single(mean(sqrt(TF_Trl(:,index==0)),2));
                        bosc.spctrm.(conditions{c}).([curPrefix,'Det']){1,t,e}(:,:) = single(relevantEpisodes);
                        bosc.spctrm.(conditions{c}).([curPrefix,'Description']) = Ranges.(curFreqName){indRange,2};
                    else
                        afs = size(cfg.eBOSC.F,2); % amount of freqs
                        bosc.spctrm.(conditions{c}).([curPrefix,'_pow']){1,t}(e,1:afs) = NaN;
                        bosc.spctrm.(conditions{c}).([curPrefixNo,'_pow']){1,t}(e,:) = single(mean(TF_Trl,2));
                        bosc.spctrm.(conditions{c}).([curPrefix,'_log10_pow']){1,t}(e,1:afs) = NaN;
                        bosc.spctrm.(conditions{c}).([curPrefixNo,'_log10_pow']){1,t}(e,:) = single(mean(log10(TF_Trl),2));
                        bosc.spctrm.(conditions{c}).([curPrefix,'_amp']){1,t}(e,1:afs) = NaN;
                        bosc.spctrm.(conditions{c}).([curPrefixNo,'_amp']){1,t}(e,:) = single(mean(sqrt(TF_Trl),2));
                        bosc.spctrm.(conditions{c}).([curPrefix,'Det']){1,t,e}(:,:) = NaN;
                        bosc.spctrm.(conditions{c}).([curPrefix,'Description']) = Ranges.(curFreqName){indRange,2};
                    end
                    clear curPrefix index
                end % sub-range
            end % freq range

            %%  clear variables
            
            MatrixDetected{c,t} = detected;
            
            clear abundance detected episodes ind* pepisode spctrm B_

        end; clear t;
    end; clear c;
    
    save([pn.boscOut, '/', IDs{id}, '_MatrixDetected_',num2str(e),'v1.mat'],'MatrixDetected')
    clear MatrixDetected;
    
end; clear e; % electrode loop

%%  keep bosc parameters

    bosc.cfg = cfg;
    bosc.BGinfo = BGinfo;
    
%%  save data
    
    save([pn.boscOut, '/' ID, '_bosc.mat'], '-struct', 'bosc');

end % function end