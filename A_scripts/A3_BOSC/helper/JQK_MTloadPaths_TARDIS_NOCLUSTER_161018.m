% Function adds paths for the Sternberg Master's Thesis data.

function [pn] = JQK_MTloadPaths_TARDIS_NOCLUSTER_161018(sessionFolder, saveFolder)

    %restoredefaultpath; clear RESTORE*
    
    pn.root       = '/Volumes/tardis/';
    %pn.fun        = '//Volumes/EEG/ConMemEEGTools/';

    %% ressources & scripts

    %pn.scripts    = '//Volumes/EEG/BOSC_Sternberg/scripts/';
    pn.data       = [pn.root, 'SternB_data/', sessionFolder];
    %pn.simData    = [pn.root, 'Processing/G_simData/', sessionFolder];
    %pn.FT         = [pn.root, 'matlab/toolboxes/fieldtrip-20160422/'];
    %pn.tools      = [pn.scripts, 'Tools/'];

    %% add paths
    
    %addpath(genpath(pn.scripts));
    %addpath(pn.FT);
    %addpath(genpath(pn.tools))
    
    %% THG's folders
    
%     addpath([pn.fun, 'FAlpha/'],'-end');
%     addpath([pn.fun, 'fnct_common/'],'-end');
%     addpath([pn.fun, 'fnct_THG/'],'-end');
    
    %% output folders
    % + create folders if they do not exist yet 

    pn.processing = [pn.root, 'processing/']; 
    if exist(pn.processing, 'dir') ~= 7
        mkdir(pn.processing);
    end;
    
    pn.param = [pn.processing, 'A_param/'];
    if exist(pn.param, 'dir') ~= 7
        mkdir(pn.param);
    end;
    
    pn.fft = [pn.processing, 'B_FFT/'];
    if exist(pn.fft, 'dir') ~= 7
        mkdir(pn.fft);
    end;
    
    pn.bosc = [pn.processing, 'C_BOSC/',saveFolder, '/',sessionFolder];
    if exist(pn.bosc,'dir') ~= 7
        mkdir(pn.bosc);
    end;
    
    pn.boscPlots = [pn.processing, 'C_BOSC/',saveFolder, '/',sessionFolder, '/Plots/'];
    if exist(pn.boscPlots,'dir') ~= 7
        mkdir(pn.boscPlots);
    end;
    
    addpath('/Volumes/EEG/ConMemEEGTools/fieldtrip-20161020/');
    
end