%    This file is part of the Better OSCillation detection (BOSC) library.
%
%    The BOSC library is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    The BOSC library is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
%
%    Copyright 2010 Jeremy B. Caplan, Adam M. Hughes, Tara A. Whitten
%    and Clayton T. Dickson.

function [pv,meanpower]=BOSC_bgfit_robust_20150606(F,B, ID, pn, e, c)
% [pv,meanpower]=BOSC_bgfit(F,B)
%
% This function estimates the background power spectrum via a
% linear regression fit to the power spectrum in log-log coordinates
% 
% parameters:
% F - vector containing frequencies sampled
% B - matrix containing power as a function of frequency (rows) and
% time). This is the time-frequency data.
%
% returns:
% pv = contains the slope and y-intercept of regression line
% meanpower = mean power values at each frequency 
%

b = robustfit(log10(F),mean(log10(B),2)');
%b = robustfit(log10(F),log10(mean(B,2))');
pv(1) = b(2);
pv(2) = b(1);

% transform back to natural units (power; usually uV^2/Hz)
meanpower=10.^(polyval(pv,log10(F))); 

% plot as output

% if nargin > 2
% 
%     h = figure; %set(gcf,'Visible','off');
%     subplot(1,2,1)
%     plot(log10(F), mean(log10(B),2)); hold on;
%     plot(log10(F), polyval(pv,log10(F)))
%     title('BG fit log-log'); xlabel('Frequency (log10 HZ)'); ylabel('Power');
%     subplot(1,2,2)
%     plot(F, 10.^mean(log10(B),2)); hold on;
%     plot(F,10.^polyval(pv,log10(F)))
%     title('BG fit'); xlabel('Frequency ( HZ)'); ylabel('Power');
%     namestr = [pn.boscPlots, num2str(ID), '_BGfit_', num2str(e), '_', num2str(c)];
% %     saveas(h, namestr, 'epsc');
% %     saveas(h, namestr, 'fig');
% %     clear namestr; close(h);
% end;
% % 
